@testable import GildedRose
import XCTest

class GildedRoseTests: XCTestCase {
    var items: [Item] = []
    var app: GildedRose = GildedRose(items: [])
    
    override func setUp() {
        super.setUp()
        items = [Item(name: "foo", sellIn: 0, quality: 0)]
        app = GildedRose(items: items)
    }

    func testFoo() throws {
        app.updateQuality()
        XCTAssertEqual(app.items[0].name, "foo")
    }
    
    func testQualityCanNeverBeNegative() throws {
        app.updateQuality()
        XCTAssertEqual(app.items[0].quality, 0)
    }
    
    // test description
    func testDescription() throws {
        let test = items[0]
        XCTAssertEqual(test.description, "foo, 0, 0")
    }
    
    func testElixirQuality() throws {
        var quality = 7
        let items =  [Item(name: "Elixir of the Mongoose", sellIn: 5, quality: quality)]
        let app = GildedRose(items: items)
        app.updateQuality()
        quality -= 1
        XCTAssertEqual(items[0].quality, quality)
    }
    
    func testElixirQualityWhenSellInDateHasPassed() throws {
        var quality = 7
        let items =  [Item(name: "Elixir of the Mongoose", sellIn: -2, quality: quality)]
        let app = GildedRose(items: items)
        app.updateQuality()
        quality -= 2
        XCTAssertEqual(items[0].quality, quality)
    }
    
    func testElixirSellIn() throws {
        var sellIn = 5
        let items =  [Item(name: "Elixir of the Mongoose", sellIn: sellIn, quality: 7)]
        let app = GildedRose(items: items)
        app.updateQuality()
        sellIn -= 1
        XCTAssertEqual(items[0].sellIn,sellIn)
    }
    
    func testBrieQuality() throws {
        let quality = 3
        let items = [Item(name: "Aged Brie", sellIn: 2, quality: quality)]
        let app = GildedRose(items: items)
        app.updateQuality()
        XCTAssertEqual(items[0].quality,quality + 1)
    }
    
    func testBrieQualityNeverIncreasesAbove50() throws {
        let items = [Item(name: "Aged Brie", sellIn: 2, quality: 49)]
        let app = GildedRose(items: items)
        app.updateQuality()
        XCTAssertEqual(items[0].quality,50)
        app.updateQuality()
        XCTAssertEqual(items[0].quality,50)
    }
    
    func testBrieSellIn() throws {
        let sellIn = 2
        let items = [Item(name: "Aged Brie", sellIn: sellIn, quality: 0)]
        let app = GildedRose(items: items)
        app.updateQuality()
        XCTAssertEqual(items[0].sellIn,sellIn - 1)
    }
    
    func testBackstagePassesQuality() throws {
        // quality goes up. Wanneer 10 of minder dagen quality * 2, 5 dagen of minder. kwaliteit * 3. 0 dagen. kwaliteit 0
        let quality = 20
        let items = [Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 15, quality: quality)]
        let app = GildedRose(items: items)
        app.updateQuality()
        XCTAssertEqual(items[0].quality,quality + 1)
    }
    
    func testBackstagePassesQualityLessThan10Days() throws {
        let quality = 10
        let items = [Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 9, quality: quality)]
        let app = GildedRose(items: items)
        app.updateQuality()
        XCTAssertEqual(items[0].quality,quality + 2)
    }
    
    func testBackstagePassesQualityLessThan5Days() throws {
        let quality = 10
        let items = [Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 4, quality: quality)]
        let app = GildedRose(items: items)
        app.updateQuality()
        XCTAssertEqual(items[0].quality,quality + 3)
    }
    
    func testBackstagePassesQualityZeroWhenSellInDatePassed() throws {
        let quality = 10
        let items = [Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 0, quality: quality)]
        let app = GildedRose(items: items)
        app.updateQuality()
        XCTAssertEqual(items[0].quality, 0)
    }
    
    func testBackstagePassesSellIn() throws {
        let sellIn = 15
        let items = [Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: sellIn, quality: 20)]
        let app = GildedRose(items: items)
        app.updateQuality()
        XCTAssertEqual(items[0].sellIn,sellIn - 1)
    }
    
    func testSulfurasQualityNeverchanges() throws {
        let quality = 80
        let items = [Item(name: "Sulfuras, Hand of Ragnaros", sellIn: 0, quality: quality)]
        let app = GildedRose(items: items)
        app.updateQuality()
        XCTAssertEqual(items[0].quality, quality)
    }
    
    func testSulfurasSellInNeverchanges() throws {
        let sellIn = 0
        let items = [Item(name: "Sulfuras, Hand of Ragnaros", sellIn: sellIn, quality: 80)]
        let app = GildedRose(items: items)
        app.updateQuality()
        XCTAssertEqual(items[0].sellIn, sellIn)
    }
    
    func testConjuredItems() throws {
        var quality = 6
        let items = [Item(name: "Conjured Mana Cake", sellIn: 3, quality: quality)]
        let app = GildedRose(items: items)
        app.updateQuality()
        quality -= 2
        XCTAssertEqual(items[0].quality, quality)
    }
}
