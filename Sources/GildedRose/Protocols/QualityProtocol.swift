//
//  QualityProtocol.swift
//  
//
//  Created by sarah nys on 22/11/2021.
//

import Foundation

protocol QualityProtocol {
    func updateFor(item: Item)
}
