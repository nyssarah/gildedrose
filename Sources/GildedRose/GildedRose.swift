
import Foundation

public class GildedRose {
    var items: [Item]
    
    public init(items: [Item]) {
        self.items = items
    }
    
    public func updateQuality() {
        for item in items {
            let quality = QualityCheck.checkQuality(for: item)
            quality.updateFor(item: item)
            updateSellIn(item: item)
        }
    }
    
    private func updateSellIn(item: Item) {
        guard item.sellIn > 0 else { return }
        item.sellIn = item.sellIn - 1
    }
}
