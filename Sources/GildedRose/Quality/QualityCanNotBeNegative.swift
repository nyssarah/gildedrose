//
//  QualityCanNotBeNegative.swift
//  
//
//  Created by sarah nys on 22/11/2021.
//

import Foundation

class QualityOfItemCanNotBeNegative: QualityProtocol {
    func updateFor(item: Item) {
        item.quality = 0
    }
}
