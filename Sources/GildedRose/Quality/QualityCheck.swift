//
//  QualityCheck.swift
//  
//
//  Created by sarah nys on 22/11/2021.
//

import Foundation

class QualityCheck {
    
    // This function is used to check which quality class should be used to return the correct quality for the item
    static func checkQuality(for item: Item) -> QualityProtocol {
        guard item.quality >= 0 else { return QualityOfItemCanNotBeNegative() }
        if item.name.lowercased().contains("conjured") {
            return ConjuredItemQualityCheck()
        } else {
            switch item.name {
            case "Aged Brie":
                return BrieQualityCheck()
            case "Backstage passes to a TAFKAL80ETC concert":
                return BackstagePassesQualityCheck()
            case "Sulfuras, Hand of Ragnaros":
                return SulfurusQualityCheck()
            default:
                return NormalItemQualityCheck()
            }
        }
    }
}
