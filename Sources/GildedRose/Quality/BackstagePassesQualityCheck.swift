//
//  BackstagePassesQualityCheck.swift
//  
//
//  Created by sarah nys on 22/11/2021.
//

import Foundation

class BackstagePassesQualityCheck: QualityProtocol {
    func updateFor(item: Item) {
        guard item.quality < 50 else { return }
        switch item.sellIn {
        // if sellIn = 0 the quality is 0 as well ( because the concert is over)
        case 0:
            item.quality = 0
        // closer to the sellIn date is more quality (+3)
        case 0...5:
            item.quality += 3
        // closer to the sellIn date is more quality ( +2)
        case 6...10:
            item.quality += 2
        default:
            item.quality += 1
        }
    }
}
