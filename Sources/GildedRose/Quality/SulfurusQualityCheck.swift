//
//  SulfurusQualityCheck.swift
//  
//
//  Created by sarah nys on 22/11/2021.
//

import Foundation

// the quality never changes
class SulfurusQualityCheck: QualityProtocol {
    func updateFor(item: Item) {
        // do nothing
    }
}
