//
//  BrieQualityCheck.swift
//  
//
//  Created by sarah nys on 22/11/2021.
//

import Foundation

// Brie gets more quality after more days
class BrieQualityCheck: QualityProtocol {
    func updateFor(item: Item) {
        guard item.quality < 50 else { return }
            item.quality += 1
    }
}
