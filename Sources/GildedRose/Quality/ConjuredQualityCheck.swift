//
//  ConjuredQualityCheck.swift
//  
//
//  Created by sarah nys on 22/11/2021.
//

import Foundation

// This is used for the conjured items. The quality degrades twice as fast
class ConjuredItemQualityCheck: QualityProtocol {
    func updateFor(item: Item) {
        item.quality -= 2
    }
}
