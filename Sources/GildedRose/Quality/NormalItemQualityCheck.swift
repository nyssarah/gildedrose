//
//  NormalItemQualityCheck.swift
//  
//
//  Created by sarah nys on 22/11/2021.
//

import Foundation

// after the sellIn dates passes the quality degrades twice as fast
class NormalItemQualityCheck: QualityProtocol {
    func updateFor(item: Item) {
        switch item.quality {
        case 0: item.quality
        case 1: item.quality - 1
        default:
            let reduction = item.sellIn <= 0 ? 2 : 1
            item.quality -= reduction
        }
    }
}
